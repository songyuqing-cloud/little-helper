import { foundry07Compatibility } from '../common.mjs';

function canvasReadyEvent(canvas) {
	const tokenVision = canvas.scene.data.tokenVision;
	if (tokenVision) {
		const playerTokens = canvas.tokens.placeables
			.filter(t => (foundry07Compatibility ? t.owner : t.isOwner) && t.actor?.hasPlayerOwner);
		const sightedTokens = playerTokens.filter(t => t.hasSight);

		if (sightedTokens.length == 0)
			ui.notifications.warn('Scene Token Vision enabled; no player tokens with vision present.');
	}
}

function sceneUpdateEvent(scene, _update, _options, _userId) {
	if (scene.id === canvas.scene?.id) // viewed scene updated
		canvasReadyEvent(canvas);
}

Hooks.on('canvasReady', canvasReadyEvent);
Hooks.on('updateScene', sceneUpdateEvent);
