import { warningSymbol, addWarning } from '../common.mjs';

function missingBitsInjection(sheet, jq, _data) {
	const actor = sheet.actor;

	// FEATS
	const featTab = jq.find('.tab.feats');
	const feats = actor.getFeatCount();
	if (feats.max !== feats.value)
		addWarning(featTab.find('.tab-footer table td').first(), game.i18n.localize('LittleHelper.Warning.FeatMismatch'));

	// SKILLS
	const sd = jq.find('.tab.skills').find('.skill-rank-info table td');
	const available = parseInt(sd.get(0)?.innerText, 10), used = parseInt(sd.get(1)?.innerText, 10);
	if (available != used)
		addWarning(sd.first().next(), game.i18n.localize('LittleHelper.Warning.SkillMismatch'));

	if (sd.length > 2)
		if (parseInt(sd[3].innerText, 10) !== parseInt(sd[4].innerText, 10))
			addWarning(sd.last(), game.i18n.localize('LittleHelper.Warning.SkillMismatch'));

	// TRAITS
	const itemWarning = (j, warning) => j.append(warningSymbol().addClass('item-warning-symbol').attr({ title: warning }))

	featTab.find('.feats-group').each(function () {
		const items = $(this).find('.item-name');
		if (items.length > 1) return;
		const label = items.find('h3').first();

		switch (label.get(0).innerText.toLowerCase()) {
			case 'feats':
				itemWarning(label, game.i18n.localize('LittleHelper.Warning.NoFeats'));
				break;
			case 'traits':
				itemWarning(label, game.i18n.localize('LittleHelper.Warning.NoTraits'));
				break;
		}
	});

	// UNUSED SPELLS
	jq.find('.tab.spellbook').find('.spell-uses .spell-slots').each(function () {
		const left = parseInt($(this).find('[data-dtype="Number"]').text().trim(), 10);
		if (left < 0 || (!this.classList.contains('spontaneous') && left > 0))
			this.classList.add('lil-warning');
	});

	const clss = jq.find('.tab.classes-body');
	let fcb = 0;
	for (let cls of clss.find('.item-list .item')) {
		const item = actor.items.get(cls.dataset.itemId);
		if (!item) continue; // Alt Sheet support
		const fc = item.data.data.fc;
		fcb += fc.alt.value + fc.hp.value + fc.skill.value;
	}
	if (fcb === 0)
		clss.find('.inventory-header .item-name')
			.append($('<label/>').addClass('lil-warning lil-box lil-size-constraint').text('No FCB on any class.'));
}

Hooks.on('renderActorSheetPF', missingBitsInjection);
