import { tooltip, hookTooltip, addWarning } from '../common.mjs';
import { Feature } from '../config.mjs';

class Slot {
	/**
	 * Maximum number of items in this slot.
	 * @type {Number}
	 */
	limit;
	/** Is the limit soft
	 * @type {Boolean}
	 */
	soft;

	constructor(limit = 1, { softLimit = false } = {}) {
		this.limit = limit;
		this.soft = softLimit;
	}

	clone() {
		return new Slot(this.limit, { softLimit: this.soft });
	}
}

/* Humanoid default */
const slotTypes = {
	belt: new Slot(1),
	belt: new Slot(1),
	body: new Slot(1),
	chest: new Slot(1),
	eyes: new Slot(1),
	feet: new Slot(1),
	hands: new Slot(1),
	head: new Slot(1),
	headband: new Slot(1),
	neck: new Slot(1),
	ring: new Slot(2),
	shoulders: new Slot(1),
	slotless: new Slot(Number.POSITIVE_INFINITY),
	wrists: new Slot(1),
	armor: new Slot(1, true),
	shield: new Slot(1),
};

function magicItemSlotInjector(sheet, jq, _data) {
	if (sheet.constructor.name.includes('Loot')) { // braindead lootsheet check
		console.debug("LITTLE HELPER | Ignoring loot sheet");
		return;
	}

	const actor = sheet.actor;
	const eq = actor.items.filter(i => i.type === 'equipment' && i.data.data.equipped);
	// Group by slot
	const grouped = eq.reduce((all, item) => {
		if (item.data.data.slot != undefined) {
			const key = item.data.data.slot;
			if (all[key] === undefined) all[key] = [];
			all[key].push(item);
		}
		return all;
	}, {});

	const info = Object.assign({}, slotTypes);
	for (let [key, value] of Object.entries(info)) info[key] = value.clone();

	for (let slot of Object.keys(slotTypes)) {
		if (grouped[slot] === undefined) continue;
		info[slot].count = grouped[slot].length;
		if (!('warn' in info[slot])) Object.defineProperty(info[slot], 'warn', { get: function () { return this.count > this.limit; } });
	}

	const tab = jq.find('.tab.inventory');
	const items = tab.find('.item');
	for (let el of items) {
		const item = actor.items.get(el.dataset.itemId);
		if (!item) continue; // Alt Sheet module support.
		const slot = item.data.data.slot;
		if (slot === undefined) continue;
		if (info[slot].warn) {
			if (grouped[slot].find(i => i.id === item.id) === undefined) continue;
			const st = $(el).find('.item-slot');
			const soft = info[slot].soft;
			st.addClass(soft ? 'lil-warning' : 'lil-error');
			let msg = game.i18n.localize('LittleHelper.Warning.TooManySlotItems').format(info[slot].count, info[slot].limit);
			if (soft) msg += "<br/>" + game.i18n.localize('LittleHelper.Clarify.NoActionNeeded');
			st.append(addWarning(tooltip(msg)));
			hookTooltip(st);
		}
	}
}

function enable() {
	Hooks.on('renderActorSheet', magicItemSlotInjector);
}

function disable() {
	Hooks.off('renderActorSheet', magicItemSlotInjector);
}

enable();

new Feature('duplicate-slots', 'Duplicate slots', 'Warn about excess magic item slot usage. Supports only default humanoid configuration.', 'items', enable, disable)
