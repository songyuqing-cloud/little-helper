import { foundry07Compatibility } from '../common.mjs';

/**
 * Anything to help avoid things being mistaken as something else.
 */

function unmistakenInjector(sheet, jq, data) {
	const tsheet = jq.get(0).closest('.app.sheet.actor');
	const t = $(tsheet).find('.window-title');
	if (!(t.length > 0)) return;

	const linked = !sheet.actor.isToken;
	if (linked) {
		// Linked but has no player owner
		if (sheet.actor.hasPlayerOwner == false) {
			t.text(t.text() + ' ' + game.i18n.localize('LittleHelper.Warning.LinkedTitleTag'));
			const n = jq.find('.tab.summary .name-xp .charname');
			n.parent().addClass('flexcol').append(
				$('<span/>').append(
					$('<i>').addClass('fas fa-link').html('&nbsp;'),
					document.createTextNode(game.i18n.localize('LittleHelper.Warning.LinkedSubtitle'))
				).addClass('lil-token-linking-notification lil-token-linked')
			);
		}
	}
	else { // undefined counts as false
		// Unlinked actor
		t.text(t.text() + ' ' + game.i18n.localize('LittleHelper.Warning.UnlinkedTitleTag'));
		const n = jq.find('.tab.summary .name-xp .charname');
		n.parent().addClass('flexcol').append(
			$('<span/>').append(
				$('<i>').addClass('fas fa-unlink').html('&nbsp;'),
				document.createTextNode(game.i18n.localize('LittleHelper.Warning.UnlinkedSubtitle'))
			).addClass('lil-token-linking-notification lil-token-unlinked')
		);
	}
}

Hooks.on('renderActorSheet', unmistakenInjector);
