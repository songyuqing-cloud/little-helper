function sceneConfigEvent(app, jq, _options) {
	/** Another warning for no initial view */
	if (app.object.data.initial == null)
		jq.find('.initial-position').addClass('lil-warning');
}

Hooks.on('renderSceneConfig', sceneConfigEvent);
