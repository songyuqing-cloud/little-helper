import { module } from './common.mjs';

export const featureKey = 'features';

export class Feature {
	/** @private */
	enabled = true;

	/** Label */
	label;
	/** Description */
	hint;
	/** Setting name */
	setting;
	/** Category for organizing */
	category;

	enable() { };
	disable() { };

	/** @private */
	static collection = [];

	/**
	 * @param key setting key
	 * @param name Display name key
	 * @param enable Callback for enabling
	 * @param disable Callback for disabling
	 * @param enabled Current state
	 */
	constructor(setting, label, hint, category, enableCallback, disableCallback) {
		this.setting = setting;
		this.label = label;
		this.hint = hint;
		this.category = category;
		this.enable = enableCallback;
		this.disable = disableCallback;

		Feature.collection[setting] = this;
	}

	toggle() {
		(this.enabled = !this.enabled) ? this.enable() : this.disable();
	}

	load() {
		const settings = game.user.getFlag(module, featureKey);
		if (this.enabled !== settings[this.setting])
			this.toggle();
	}

	save() {
		const settings = this.load();
		settings[this.setting] = this.enabled;
		game.user.setFlag(module, featureKey, settings);
	}
}
