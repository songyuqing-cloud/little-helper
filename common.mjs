export const module = 'koboldworks-pf1-little-helper';

export function enrichRoll(roll, formula, label) {
	const a = document.createElement('a');
	a.classList.add('inline-roll', 'inline-result');
	a.title = formula;
	a.dataset.roll = escape(JSON.stringify(roll));
	a.innerHTML = `<i class="fas fa-dice-d20"></i> ${label}`;
	return a;
}

export var foundry07Compatibility = false;
Hooks.once('init', () => {
	foundry07Compatibility = game.data.version.split('.', 3)[1] === '7';
	if (foundry07Compatibility) console.log("LITTLE HELPER | Compatibility Mode | Foundry 0.7.x");
});

export const signNum = (num) => num >= 0 ? `+${num}` : `${num}`;

export const warningSymbol = () => $('<i/>').addClass('fas fa-exclamation-triangle');

export const addWarning = (j, warning) => j.addClass('lil-warning').prepend(constructWarning(warning));
export const addPostWarning = (j, warning) => {
	const symbol = constructWarning(warning);
	j.addClass('lil-warning').after(symbol);
	return symbol;
}

export const constructWarning = (warning) => warningSymbol().attr({ title: warning }).addClass('warning-symbol-buffer');

export const tooltip = (content) => $('<span/>').addClass('lil-tooltip').html(content);

export const isIncompatibleSheet = (sheet) => sheet.constructor.name.startsWith('AltActor'); // zenvy's alt sheet

// Hook parent
export const hookTooltip = (el) => {
	const tt = el.find('.lil-tooltip').first();
	let hovering = false;
	el.mousemove(function (e) {
		if (!hovering) return;
		tt.css({left: `${e.clientX + 14}px`, top: `${e.clientY - 24}px` });
	});
	el.hover(
		function () {
			hovering = true;
			tt.stop().fadeIn(200);
		},
		function () {
			hovering = false;
			tt.stop().fadeOut(200);
		}
	);
}
