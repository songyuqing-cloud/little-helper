function renderCompendium(directory, jq, options) {
	const dir = jq.find('.directory-list').first();
	const packs = dir.find('.compendium-pack .compendium-footer');
	const book = () => {
		const b = document.createElement('i');
		b.classList.add('fas', 'fa-book');
		return b;
	}
	const sources = packs.find('span');
	sources.each(function () {
		if (this.classList.contains('entity-type')) {
			this.remove();
			return;
		}
		const b = book();
		this.textContent.match(/^\s*\((.*)\)$/g);
		b.title = RegExp.$1;
		this.after(b);
		this.remove();
	});
}

Hooks.on('renderCompendiumDirectory', renderCompendium);
