function tablessSummary(sheet, jq, _opts) {
	const s = jq.find('.tab.summary');
	const st = s.find('.tab[data-group="subdetails"]');
	const tabs = s.find('.sheet-navigation.tabs.subtabs');
	tabs.hide();

	st.css({ height: 'unset', display: 'block' });
	s.find('.tab[data-tab="classes"]').prepend($('<h2>').text(game.i18n.localize('PF1.Classes')));
}

Hooks.on('renderActorSheetPF', tablessSummary);
