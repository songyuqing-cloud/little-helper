// Display current CL at spellbook name.
function itemCountInjector(_sheet, jq, _data) {
	jq.find('.tab.spellbook')
		.find('.spellbook-header')
		.each(function () {
			const jq_ = $(this);
			const spells = jq_.next('.item-list').find('div.item-name');
			const count = Math.max(0, spells.length);
			jq_.find('.item-name').first().append($('<label/>').text(`Known: ${count}`).addClass('lil-spells-known'));
		});

	jq.find('.tab.feats')
		.find('.inventory-header')
		.each(function () {
			const jq_ = $(this);
			const feats = jq_.next('.item-list').find('div.item-name');
			const count = Math.max(0, feats.length);
			const name = jq_.find('.item-name').first();
			const t = duplicate(name.text());

			name.parent().prepend($('<div/>').addClass('item-name flexrow')
				.append(
					name.removeClass('item-name flexrow'),
					$('<label/>').text(`Count: ${count}`).addClass('lil-feats-possessed')
				));
		});
}

Hooks.on('renderActorSheetPF', itemCountInjector);
