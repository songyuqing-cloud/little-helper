import { Feature } from '../config.mjs';

// Replaces the die roll icon for many things with info symbol to better signal that they're not for acting.

/*
const feature = new Feature(
	'skill-roll-relocate',
	'Koboldworks.LittleHelper.Feature.SkillRollRelocate',
	() => { },
	() => { },
	true,
);
*/

function skillRollHook(_sheet, jq, _data) {
	const skills = jq.find('.tab.skills[data-tab="skills"]');

	skills.find('.skills-list .skill, .skills-list .sub-skill').each(function () {
		const name = $(this).find('.skill-name').first();
		const roller = name.find('.rollable').first().addClass('lil-right-buffer').css({ 'margin-right': '0.5em' });
		name.find('h4').addClass('lil-left-buffer').css({ 'margin-left': '0.5em' });

		// ensure only one spacer exists
		const spacer = name.find('.lil-spacer');
		if (spacer.length === 0) name.append($('<div/>').addClass('lil-spacer'));

		name.append(roller);
	});
}

Hooks.on('renderActorSheetPF', skillRollHook);
