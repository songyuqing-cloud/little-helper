// Display current CL at spellbook name.
function casterLevelInjection(sheet, jq, _data) {
	const spellbooks = jq.find('.tabs.spellbooks[data-group="spellbooks"]');
	const tabs = spellbooks.find('.item');
	const rd = sheet.actor.getRollData();
	for (let tab of tabs) {
		const book = tab.dataset.tab;
		const cl = getProperty(rd, `spells.${book}.cl.total`);
		$(tab).append(document.createTextNode(` [${cl}]`));

		// no conc total is available easily
		//jq.find('.spellcasting-concentration label').append($('<span/>').text(` +${concBonus}`));
		// CL + Ability + Modifiers
	}
}

Hooks.on('renderActorSheetPF', casterLevelInjection);
