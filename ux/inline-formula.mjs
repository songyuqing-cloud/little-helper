/**
 * Adds formula display on top of the inline roll expansion.
 */

function mutationCallback(mutList, _observer) {
	function mutationHandler(mut) {
		switch (mut.type) {
			case 'childList':
				for (let node of mut.addedNodes) {
					if (node.classList?.contains('dice-tooltip')) {
						const formulaEl = document.createElement('div');
						formulaEl.append(document.createTextNode(mut.target.title));
						formulaEl.classList.add('lil-dice-tooltip-source');
						node.prepend(formulaEl, document.createElement('hr'));
					}
				}
				break;
		}
	}

	Array.from(mutList).forEach(mutationHandler);
}

const observer = new MutationObserver(mutationCallback);

Hooks.once('renderChatLog', (_, jq) => observer.observe(jq.get(0).querySelector('#chat-log'), { subtree: true, childList: true, attributes: false }));
