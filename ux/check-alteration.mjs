import { signNum } from '../common.mjs';

/* 2 decimals if any are present, otherwise 0 */
const limitPrecision = (num) => (num !== Math.floor(num)) ? (Math.round(num * 100) / 100) : num;

// For 0.77.24 and older
function chatMessageCompactor(_cm, jq, _data) {
	const roll = jq.find('.dice-roll');
	const formula = roll.find('.dice-formula');
	const total = roll.find('.dice-total');

	if (roll.length <= 0 || formula.length <= 0 || total.length <= 0) return;

	formula.append($('<span/>').text(' = ')).append(
		$('<span/>').text(total.text())
			.attr({ class: total.attr('class') }).addClass('compact')).addClass('enlarged');
	total.remove();
}

// For 0.78.x and newer
function chatMessageCheckExpander(cm, jq, _options) {
	const roll = cm.data.roll ? cm.roll : null;
	if (roll == null) return;
	if (roll.dice?.length !== 1) return; // leave weird ones alone

	const rollEl = jq.find('.dice-roll'),
		total = rollEl.find('.dice-total'),
		d20 = rollEl.find('.roll.die.d20').get(0);
	if (total.length > 0 && d20) {
		const rawRollBonus = limitPrecision(roll.total - parseInt(d20.innerText, 10));
		const ntRoll = document.createElement('span'),
			ntResult = document.createElement('span');
		ntRoll.textContent = d20.innerText;
		ntRoll.classList.add('lil-raw-roll');
		ntResult.classList.add('lil-roll-total');
		const tDom = total.get(0);
		// Move certain classes into child elements
		for (let c of ['success', 'failure']) {
			if (tDom.classList.contains(c)) {
				tDom.classList.remove(c);
				ntRoll.classList.add(c);
			}
		}
		ntResult.textContent = tDom.innerText;
		tDom.innerText = '';
		const ntBonus = document.createElement('span');
		ntBonus.innerText = `(${signNum(rawRollBonus)})`;
		ntBonus.classList.add('lil-roll-bonus');

		total.append(ntRoll, document.createTextNode(' '), ntBonus, document.createTextNode(' ⇒ '), ntResult);
	}
}

let initialCMs = [];

function collectOldMessages(cm, jq, _data) {
	initialCMs.push([ cm, jq ]);
}

Hooks.on('renderChatMessage', collectOldMessages);

Hooks.once('ready', () => {
	Hooks.off('renderChatMessage', collectOldMessages);

	const oldVersion = !isNewerVersion(game.system.data.version, '0.77.24');
		for (let cm of initialCMs) {
			if (oldVersion) chatMessageCompactor(...cm);
			else chatMessageCheckExpander(...cm);
		}

		if (oldVersion) Hooks.on('renderChatMessage', chatMessageCompactor);
		else Hooks.on('renderChatMessage', chatMessageCheckExpander);

	initialCMs = null;
	collectOldMessages = null;
});
