function redesignChatMessageHook(cm, jq, _data) {
	const item = cm.itemSource;
	if (!item) return;
	if (item.hasDamage) {
		const buttonGroups = jq.find('.card-buttons').removeClass('flexcol').addClass('flexrow');

		buttonGroups.find('button').each(function () {
			const t = this.textContent.toLowerCase();
			switch (t) {
				case 'apply':
					this.textContent = 'Full';
					break;
				case 'apply half':
					this.textContent = 'Half';
					break;
				case 'recover':
					this.textContent = 'Chance'
					break;
				case 'recover (force)':
					this.textContent = 'All';
					break;
			}

			// 0.77.x fix
			if (this.dataset.action === 'save') {
				this.style.width = 'auto';
				this.style.flex = '100%';
			}
		});

		buttonGroups.find('button[data-action=recoverAmmo]').closest('.card-button-group').find('label').text('Recover Ammo');

		buttonGroups.each(function () {
			const groups = $(this).find('.card-button-group');
			const splits = groups.length - 1;
			const splitter = () => $('<div/>').css({ width: '5px', flex: '0 0 3px' });
			if (splits > 0) groups.first().after(splitter());
			if (splits > 1) groups.last().before(splitter());
		})
	}
}

// ----- HOOKS and delayed processing of old messages
// Hacky fix for: https://gitlab.com/Furyspark/foundryvtt-pathfinder1/-/issues/782

let initialCMs = [];

function collectOldMessages(cm, jq, _data) {
	initialCMs.push([ cm, jq ]);
}

Hooks.on('renderChatMessage', collectOldMessages);

Hooks.once('ready', () => {
	Hooks.off('renderChatMessage', collectOldMessages);
	for (let cm of initialCMs) {
		try {
			redesignChatMessageHook(...cm);
		}
		catch (err) {
			console.error(err);
		}
	}
	Hooks.on('renderChatMessage', redesignChatMessageHook);
	initialCMs = null;
	collectOldMessages = null;
});
