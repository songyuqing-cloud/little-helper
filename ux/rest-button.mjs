import { isIncompatibleSheet } from '../common.mjs';

/*
import { Feature } from '../config.mjs';

new Feature('rest-button-move',
	'Rest button move', 'Move rest button aside.',
	'actor-sheet',
	() => Hooks.on('renderItemSheetPF', moveRestButton),
	() => Hooks.off('renderItemSheetPF', moveRestButton));
*/

function moveRestButton(sheet, jq, _) {
	if (isIncompatibleSheet(sheet)) return;
	const summary = jq.find('.tab.summary');
	const qa = summary.find('.actor-quick-actions');
	if (qa.length === 0) return; // bail if we can't find things
	const raceRow = summary.find('.race-container').first().parent();
	raceRow.append(qa.parent());
	qa.parent().get(0).style.flex = '1';
	qa.css({ 'justify-content': 'flex-end' })
}

Hooks.on('renderActorSheetPF', moveRestButton);
