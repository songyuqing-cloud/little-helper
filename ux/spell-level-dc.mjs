// Display current CL at spellbook name.
function spellLevelDCInjector(sheet, jq, _data) {
	const rd = sheet.actor.getRollData();
	jq.find('.tab.spellbook section .tab')
		.each(function () {
			const spells = sheet.actor.data.data.spells[this.dataset.tab];
			rd.ablMod = sheet.actor.data.data.abilities[spells.ability].mod;
			let slOffset = spells.autoSpellLevelCalculation ? spells.hasCantrips ? 0 : 1 : 0;
			$(this).find('.spellbook-header')
				.each(function (index) {
					rd.sl = index + slOffset;
					const spl = $(this).next().find('.item[data-item-id]').first();
					if (spl.length) {
						const splItem = sheet.actor.items.get(spl.get(0).dataset.itemId);
						if (splItem) rd.sl = splItem.spellLevel;
					}
					const roll = RollPF.safeRoll(spells.baseDCFormula, rd, 'spell DC');
					$(this).find('.item-name').first().append($('<label/>').text(`DC: ${roll.total}`).attr({ title: roll.formula }).addClass('lil-spell-dc'));
				});
		});
}

Hooks.on('renderActorSheetPF', spellLevelDCInjector);
