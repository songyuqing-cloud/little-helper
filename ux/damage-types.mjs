import { module } from '../common.mjs';

class Damage {
	key = null;
	label = null;
	match = null;
	icon = null;
	color = null;
	glow = null;

	constructor(key, label, match, icon = null, color = null, glow = null) {
		this.key = key;
		this.label = label;
		this.match = match;
		this.icon = icon;
		this.color = color;
		this.glow = glow;
	}
}

const DamageType = {
	fire: new Damage('fire', 'Fire', /\b(?:fire|flame|flaming)\b/i, ['ra','ra-fire-symbol'], '#efc178', 'orange'), // 'fas fa-fire-alt' // 'fas fa-meteor'
	cold: new Damage('cold', 'Cold', /\b(?:cold|frost|freezing)\b/i, ['ra','ra-snowflake'], '#65ffff', 'white'),
	acid: new Damage('acid', 'Acid', /\bacid\b/i, ['ra','ra-acid'], 'greenyellow', 'lime'),
	electric: new Damage('electric', 'Electric', /\b(?:electric|electricity|electrifying|lightning)\b/i, ['ra','ra-lightning-storm'], 'lightskyblue', '#6475ff'),
	sonic: new Damage('sonic', 'Sonic', /sonic/i, ['ra','ra-horn-call'], 'gold', '#64ffb3'), // 'ra ra-ringing-bell' // 'fas fa-music' // 'ra ra-horn-call'
	bludgeon: new Damage('bludgeon', 'Bludgeon', /\b(?:[Bb]|[Bb]ludgeon|[Bb]ludgeoning|[Bb]lunt)\b/, ['ra','ra-flat-hammer'], 'white', 'rgba(255,0,0,0.8)'),
	piercing: new Damage('piercing', 'Piercing', /\b(?:[Pp]|[Pp]ierce|[Pp]iercing)\b/, ['ra','ra-spear-head'], 'white', 'rgba(255,0,0,0.8)'),
	slashing: new Damage('slashing', 'Slashing', /\b(?:[Ss]|[Ss]lashing|[Ss]lash|[Cc]utting)\b/, ['ra','ra-sword'], 'white', 'rgba(255,0,0,0.8)'),
	precision: new Damage('precision', 'Precision', /\b(precision|sneak attack)\b/i, ['ra','ra-on-target'], '#ff0', 'white'),
	magic: new Damage('magic', 'Magic', /\bmagic\b/i, ['ra','ra-fairy-wand'], 'fuchsia', 'cyan'), // '#a155ff',
	force: new Damage('magic', 'Force', /\bforce\b/i, ['ra','ra-focused-lightning'], '#ff8ff1', 'cyan'),
	untyped: new Damage('untyped', 'Untyped', /\buntyped\b/i, ['fas','fa-question-circle'], 'white', 'yellow'),
	healing: new Damage('healing', 'Healing', /\b(?:heal|healing)\b/, ['ra','ra-health'], 'orangered', 'pink'), // 'fas fa-first-aid'
	positive: new Damage('positive', 'Positive Energy', /\bpositive\b/i, ['fas','fa-seedling'], 'white', 'lime'), // 'fas fa-seedling' // 'ra ra-regeneration'
	negative: new Damage('negative', 'Negative Energy', /\bnegative\b/i, ['fas','fa-skull'], 'white', 'lime'),
	bleed: new Damage('bleed', 'Bleed', /\b(bleed|bleeding)\b/i, ['ra','ra-droplet'], '#fb4a4a', 'white'),
	splash: new Damage('splash', 'Splash', /\bsplash\b/i, ['ra','ra-bomb-explosion'], 'orange', 'yellow'),
}

const typeDelim = new RegExp(/\b(\/|or|and)\b/, "g");
const delimExact = new RegExp(/^(\/|or|and)$/);
function identifyType(text) {
	const strings = text.trim().toLowerCase().split(typeDelim);
	const rv = [];
	//console.log(text, strings);
	for (let str of strings) {
		if (delimExact.test(str)) continue; // skip delimiters themselves.
		//console.log(str);
		for (let d of Object.values(DamageType)) {
			//console.log(str, "===", d.key, d.match);
			if (d.match.test(str)) rv.push(d);
		}
	}
	if (rv.length) return rv;
	return [DamageType.untyped];
}

function insertSymbol(_index, el) {
	const types = identifyType(el.textContent);
	//console.log("Type:", types.map(t => t.label).join(', '));
	for (let type of types) {
		if (el.textContent.length === 1) el.textContent = type.label;
		if (type.icon) {
			const icon = document.createElement('i');
			icon.classList.add(...type.icon, 'lil-damage-type');
			el.append(icon);
			if (type.color?.length > 0) icon.style.color = `${type.color}`;
			if (type.glow?.length > 0) icon.style.textShadow = `0 0 5px ${type.glow}`;
		}
	}
}

// Discard bad things. Accept only every second TD.
function acceptInstance(index, el) {
	return (index + 1) % 2 === 0 && $(el).find('.inline-roll').length === 0 && $(el).prev().text().length > 0;
}

function insertDamageTypes(cm, jq, data) {
	const item = cm.itemSource;
	if (!item || !item.hasDamage) return;
	jq.find('tr')
		.each(function () {
			$(this).find('td').filter(acceptInstance).each(insertSymbol);
		});
}

// ----- HOOKS and delayed processing of old messages
// Hacky fix for: https://gitlab.com/Furyspark/foundryvtt-pathfinder1/-/issues/782

let initialCMs = [];

function collectOldMessages(cm, jq, _data) {
	initialCMs.push([ cm, jq ]);
}

Hooks.on('renderChatMessage', collectOldMessages);

Hooks.once('ready', () => {
	Hooks.off('renderChatMessage', collectOldMessages);
	for (let cm of initialCMs) {
		try {
			insertDamageTypes(...cm);
		}
		catch (err) {
			console.error(err);
		}
	}
	Hooks.on('renderChatMessage', insertDamageTypes);
	initialCMs = null;
	collectOldMessages = null;

	function preload(url) {
		const link = document.createElement('link');
		link.rel = 'prefetch';
		link.href = url;
		link.as = 'font';
		document.head.append(link);
	}

	preload(`modules/${module}/css/ra/fonts/rpgawesome-webfont.woff2`);
});
