// Adds little display of what action an item uses and if it counts as action within the system

function injectActionDisplay(sheet, jq, options) {
	const item = sheet.item;
	if (!item) return;

	const h = jq.find('.header-details .summary').first();
	if (h.length === 0) return; // shouldn't happen

	const checkBox = (checked) => $(`<i class="far fa-${checked ? 'check-' : ''}square"></i>`).addClass('icon');
	const line = () => $('<li>').addClass('item-detail');
	const actionLabel = () => {
		const type = item.data.data.activation?.type;
		const rv = [];
		if (type?.length > 0) {
			rv.push(' [');
			const cost = item.data.data.activation.cost;
			if (cost > 1) rv.push(`${cost} `);
			rv.push(CONFIG.PF1.abilityActivationTypes[type])
			rv.push(']');
			return document.createTextNode(rv.join(''));
		}
		return null;
	}

	const b = $('<ol/>').addClass('kbwks-item-details');
	if (!['race','class'].includes(item.data.type)) {
		b.append(
			line().addClass('lil-property action').append(
				checkBox(item.hasAction),
				document.createTextNode('Action'),
				actionLabel()
			)
		);
		if (game.user.isGM) {
			b.append(line().addClass('lil-property identified').append(
				checkBox(!item.showUnidentifiedData),
				document.createTextNode('Identified')
			))
		}
	}

	h.after($('<div/>').addClass('kbwks-flow-break'), b);
}

Hooks.on('renderItemSheetPF', injectActionDisplay);
