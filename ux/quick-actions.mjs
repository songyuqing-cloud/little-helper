const actualChargeCount = (charges, cost) => Math.floor(charges / cost);

function quickActionsInjector(sheet, jq, data) {
	const qa = jq.find('.tab.summary .quick-actions').first();

	qa.find('.item').each(function () {
		const id = this.dataset.itemId;
		//const type = this.dataset.type;
		const item = sheet.actor.items.get(id);
		if (!item) return; // Alt Sheet module support.

		//console.log(id, type, item, this);

		if (item.isCharged) {
			if (item.chargeCost > 0) {
				const uses = actualChargeCount(item.charges, item.chargeCost),
					max = actualChargeCount(item.maxCharges, item.chargeCost);

				$(this).append($('<charge/>').addClass('kbwks-use-counter').append(
					$('<span>').text(uses).addClass('remaining-uses'),
					$('<span/>').text('/').addClass('delimiter'),
					$('<span/>').text(max).addClass('maximum-uses'),
				));
			}
			else if (item.chargeCost < 0) {
				$(this).append($('<charge/>').addClass('kbwks-use-counter').append(
					$('<span/>').text(`+${-item.chargeCost}`).addClass('charge-increase')
				));
			}
		}
		else {
			const unwrapItemCharges = (i) => actualChargeCount(i?.charges ?? 0, i?.chargeCost ?? 1);
			const ammo = item.data.data.links.ammunition?.map(l => unwrapItemCharges(sheet.actor.items.get(l.id)));
			if (ammo) {
				let total = ammo.reduce((a,b) => a+b, 0);
				$(this).append($('<charge/>').addClass('kbwks-use-counter').append(
					$('<span>').text(total).addClass('remaining-uses')
				));
			}
		}

	});
}

Hooks.once('ready', () => {
	// implemented in core after 0.77.23
	if (!isNewerVersion(game.system.data.version, '0.77.23')) {
		console.log("LITTLE HELPER | Quick Actions | Enabling");
		Hooks.on('renderActorSheetPF', quickActionsInjector);
	}
});
