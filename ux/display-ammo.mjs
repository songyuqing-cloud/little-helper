function injectAmmoDisplay(sheet, jq, options) {
	const item = sheet.item;
	if (!item) return;
	if (!(item.data.data.links?.ammunition?.length > 0)) return;

	const h = jq.find('.header-details .summary').first();
	if (h.length === 0) return; // shouldn't happen

	let totalAmmo = 0;

	const actor = sheet.actor;
	for (let ammo of item.data.data.links.ammunition) {
		const ad = actor.items.get(ammo.id);
		if (!ad) continue; // bad link?
		totalAmmo += Math.max(0, ad.data.data.quantity);
	}

	h.append($('<li>').text(game.i18n.localize('PF1.AmmoRemaining').format(totalAmmo)));
}

Hooks.on('renderItemSheetPF', injectAmmoDisplay);
