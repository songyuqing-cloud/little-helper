const gulp = require('gulp');
const gulp_less = require('gulp-less');

const lessWatch = ['./less/*.less']; // for watching
const lessOrigin = './less/little-helper.less'; // conversion start
const cssDestination = './css/';// Output

const less = () => gulp.src(lessOrigin).pipe(gulp_less()).pipe(gulp.dest(cssDestination));
gulp.task('less', less);

gulp.task('watch', async (cb) => {
	gulp.watch(lessWatch, less);
	cb();
});

gulp.task('default', gulp.parallel('less'));
