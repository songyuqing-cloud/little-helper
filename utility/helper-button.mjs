// Doesn't do anything yet
// What _could_ it do?

import { module } from '../common.mjs';

const template = `modules/${module}/template/diagnosis.hbs`;
Hooks.once('init', () => { loadTemplates([template]); });

async function showLittleHelper(ev, actor) {
	const data = { actor }; // stub

	new Dialog(
		{
			title: 'LITTLE HELPER',
			content: await renderTemplate(template, data),
			buttons: {
				dismiss: {
					label: 'OK',
					icon: '<i class="fas fa-power-off"></i>',
				},
			},
			default: 'dismiss'
		},
		{
			width: 360,
		}
	).render(true);
}

function injectHelperButton(sheet, buttons) {
	if (!sheet.isEditable) return;

	buttons.unshift({
		class: 'mkah-little-helper-button',
		icon: 'fas fa-prescription-bottle-alt',
		label: game.i18n.localize('LittleHelperPF1.Button'),
		onclick: async (ev) => showLittleHelper(ev, sheet.actor),
	});
}

/*
Hooks.on('getActorSheetPFHeaderButtons', injectHelperButton);
*/
