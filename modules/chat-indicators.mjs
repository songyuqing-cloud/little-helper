// https://foundryvtt.com/packages/chat-indicators

// Removes the redundant "(GM) Roll" bit from indicators

function accessibilityCompactor(cm, jq, _opts) {
	const r = jq.find('.chat-mode-indicator');
	if (r.length === 0) return;
	const dom = r.get(0);
	dom.textContent = dom.textContent.replace(/ (?:gm )roll$/i, "");
}

let initialCMs = [];

function collectOldMessages(cm, jq) {
	initialCMs.push([ cm, jq ]);
}

Hooks.on('renderChatMessage', collectOldMessages);

Hooks.once('ready', () => {
	// Disable in case the module isn't there
	Hooks.off('renderChatMessage', collectOldMessages);
	if ((game.modules.get('chat-indicators')?.active ?? false)) {
		for (let cm of initialCMs)
			accessibilityCompactor(...cm);
		Hooks.on('renderChatMessage', accessibilityCompactor);
	}
	initialCMs = null;
	collectOldMessages = null;
});
