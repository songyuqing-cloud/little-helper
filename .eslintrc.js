module.exports = {
	parser: '@babel/eslint-parser',
	env: {
		node: false,
		jquery: true,
		browser: true,
		es2020: true,
		es6: true,
		es7: true,
		es2021: true,
	},
	extends: [ 'eslint:recommended', "@typhonjs-fvtt/eslint-config-foundry.js" ],
	parserOptions: {
		ecmaVersion: 2021, // = 12; redundant with es2021 option
		sourceType: 'module',
		ecmaFeatures: {
			globalReturn: false,
			impliedStrict: true,
		}
	},
	rules: {
		'class-methods-use-this': ['warn', {
			exceptMethods: ['getData', '_updateObject'],
		}],
		'no-unused-vars': ['warn', {
			argsIgnorePattern: '^_'
		}],
		'no-undef': ['warn', { typeof: true }],
		'no-shadow': ['error', { builtinGlobals: true, hoist: 'all', allow: ['event'] }],
		strict: ['warn', 'global'],
	},
}
