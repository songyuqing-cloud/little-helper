# Koboldworks Little Helper for Pathfinder 1e

Bunch of little tweaks, notifications, opinionated improvements, etc. that are too small to exist on their own.

This probably goes very well with a lot of the custom CSS displayed here: https://gitlab.com/mkahvi/foundry-macros/-/snippets/2076956

WARNING: Due to limited information available in PF1e system, this module's functionality is limited to English.

## Features

- Pathfinder 1e specific
  - Display caster level on spellbook title to ease with problems relating to CL alterations.
  - Display little notes for checks.
    - Skill checks, ability checks, saving throws, concentration, etc.
  - Warn about mismatched skill point & feat usage
  - Warn about lacking traits & feats
  - Various checks chatcards are significantly smaller (0.77.24 and older).
  - Various checks chatcards are more detailed (0.78.x and older).
  - Highlights spells left to prepare, if any.
  - Highlights overuse of spells if spontaneous.
  - Display spells known per level.
  - Display base spell DC per level.
  - Display feat, trait, etc. counts.
  - Display warning about token being unlinked.
  - Dispaly natural reach near size and stature.
  - Reformats attack cards to show little symbol for each damage type.
  - Charge origin
  - Attack button redesign
  - Me too roll button for chat cards – for quickly and easily assisting with checks other characters do or otherwise doing the same check.
- System agnostic features
  - Document ID button at top of most sheets.
  - Compress compendium tab for easier reading of the info that matters.
  - Warn about scene lacking initial view.
  - Warn about scene lacking player tokens with vision when token vision is enabled.

## Screens

Smaller _checks_ with little helper tags (partially disabled with PF1e 0.78 and newer):  
![Checks](./img/screencaps/check.png)

0.78.x and later have instead bigger checks:  
![Newer Checks](./img/screencaps/check-expanded.png)

Mismatched _feat_ usage:  
![Feats](./img/screencaps/feats.png)

Lacking _feats_ & _traits_:  
![Feats and Traits](./img/screencaps/feats-and-traits.png)

Mismatched _skill ranks_:  
![Skills](./img/screencaps/skill-ranks.png)

Editing _unlinked_ tokens:  
![Unlinked](./img/screencaps/unlinked.png)

Natural reach:  
![Natural Reach](./img/screencaps/natural-reach.png)

Quick action uses (disabled for PF1 0.77.24 and newer where this is built-in):  
![Quick Action Uses](./img/screencaps/quick-action-uses.png)

Damage type icons:  
![Damage Type Icons](./img/screencaps/damage-types.png)

Charge configuration origin:  
![Charge Origin](./img/screencaps/charge-origin.png)

Spells known and base DC:  
![Spells known and base DC](./img/screencaps/spell-dc-and-known.png)

Attack card button redesign:  
![Redesign](./img/screencaps/attack-redesign.png)

Skill roll button reposition:  
![Reposition](./img/screencaps/skill-roll-button.png)

Action type display:  
![Action Type](./img/screencaps/action-type-display.png)

Inline Roll Formula:  
![Inline Roll Formula](./img/screencaps/inline-roll-formula.png)

Me too roll:  
![Me Too Roller](./img/screencaps/me-too.png)

Compressed compendiums:  
![Compendiums](./img/screencaps/compress-compendiums.png)

Document ID:  
![Document ID](./img/screencaps/document-id.png)

## Planned features

- Common formula access
- Warn about lacking FCB

## Configuration

None.

## Limitations

English. Limited functionality outside of it.

## Compatibility

### Alt Sheet

CSS fixes:
```css
.lil-box.natural-reach { display: none; }
```

## Install

Manifest URL: https://gitlab.com/koboldworks/pf1/little-helper/-/raw/master/module.json

**WARNING**: Above manifest gives you the repository master branch, not a proper versioned release.  
Currently the version is being incremented irregularly, so if you want to update the module, you need to do so manually by re-installing it.  
The version number currently includes date of the release (local to me) in format of YYYYMMDDHH.

**UPDATING**: You may need to do so manually by re-installing it. Update checks within Foundry will not work usually due to version number not being updated consistently.

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE), and by extension under [FVTT's Module Development License](https://foundryvtt.com/article/license/).

## Credits

Uses [RPG-Awesome](https://nagoshiashumari.github.io/Rpg-Awesome/), which is distributed under the BSD 2-clause license.
