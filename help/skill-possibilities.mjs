function skillPossibilityInjector(sheet, jq, opts) {
	const actor = sheet.actor;
	const tab = jq.find('.tab[data-tab="skills"]');
	console.log(tab);
	const skills = tab.find('li.skill');
	console.log(skills);
	skills.each(function () {
		const skill = this.dataset.skill;
		console.log(skill);
		const ski = actor.getSkillInfo(skill);
		console.log(ski);
		const mod = ski.bonus;
		switch (skill) {
			case 'acr':
				const speedMod = Math.floor((actor.data.data.attributes.speed.land.total - 30) / 10) * 4;
				const dMin = 1 + mod + speedMod,
					dMax = 20 + mod + speedMod;
				const hMin = Math.floor((1 + mod) / 4),
					hMax = Math.floor((20 + mod) / 4);
				const s = document.createElement('div');
				s.classList.add('flexrow');
				s.classList.add('lil-skill-possibility')
				const label = document.createElement('span'),
					distances = document.createElement('span');
				label.innerHTML = 'Distance:<br/>Height:<br/>';
				distances.innerHTML = `${dMin} – ${dMax}<br/>${hMin} – ${hMax}`;
				s.append(label, distances);

				const name = $(this).find('.skill-name');

				let spacer = name.find('.lil-spacer');
				if (spacer.length === 0) {
					spacer = $('<div/>').addClass('lil-spacer');
					name.append(spacer);
				}

				spacer.after(s);
				break;
		}
	});
}

Hooks.on('renderActorSheet', skillPossibilityInjector);
