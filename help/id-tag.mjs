import { module, foundry07Compatibility } from '../common.mjs';
import { Feature } from '../config.mjs';

const getSheetId = (sheet) => {
	if (sheet instanceof Compendium)
		return `${sheet.metadata.package}.${sheet.metadata.name}`;
	else
		return foundry07Compatibility ? sheet.entity.id : sheet.document.id;
}

async function copyIDToClipboard(sheet) {
	const id = getSheetId(sheet);
	if (id?.length > 0) {
		await navigator.clipboard.writeText(id);
		ui.notifications.info(`"${id}" copied to clipboard`);
	}
	else
		ui.notifications.warn('ID not found.');
}

function injectIDButton(sheet, buttons) {
	buttons.unshift({
		class: `${module}-id-button`,
		icon: 'far fa-address-card',
		label: game.i18n.localize('LittleHelper.UI.IDButton'),
		onclick: async _ => copyIDToClipboard(sheet),
	});
}

const events = [
	'getActorSheetHeaderButtons',
	'getItemSheetHeaderButtons',
	'getJournalSheetHeaderButtons',
	'getRollTableConfigHeaderButtons',
	'getCompendiumHeaderButtons',
	'getSceneConfigHeaderButtons',
	'getCombatantConfigHeaderButtons'
];

function enable() {
	for (let ev of events)
		Hooks.on(ev, injectIDButton);
}

function disable() {
	for (let ev of events)
		Hooks.off(ev, injectIDButton);
}

enable();

new Feature('sheet-id', 'Sheet ID', 'Display button on sheet header to copy relevant ID to clipboard.', 'ui', enable, disable);
