function clarifyTokenSettings(tokenConfig, jq, options) {
	const dSight = jq.find('input[name="dimSight"]:first');
	dSight.after($('<p class="notes">').text(game.i18n.localize('LittleHelper.Foundry.Sight.Dim.Hint')));
	const bSight = jq.find('input[name="brightSight"]:first');
	bSight.after($('<p class="notes">').text(game.i18n.localize('LittleHelper.Foundry.Sight.Bright.Hint')));

	/*
	const llv = jq.find('input[name="flags.pf1.lowLightVision"]');//.parent();
	const llvmult = jq.find('input[name="flags.pf1.lowLightVisionMultiplier"]');//.parent();
	console.log(llv, llvmult);
	//llv.after(llvmult.children());
	*/
}

Hooks.once('ready', () => {
	Hooks.on('renderTokenConfig', clarifyTokenSettings);
});
