import { addPostWarning } from '../common.mjs';

const rules = {
	matchCount: {
		parenthesis: {open:'(', close:')', warning: 'LittleHelper.Warning.MismatchedQuantity'},
		squareBrackets: { open: '[', close: ']', warning: 'LittleHelper.Warning.MismatchedQuantity' },
		curlyBrackets: {open:'{', close:'}', warning: 'LittleHelper.Warning.MismatchedQuantity'}
	},
	followups: {
		variable: { match: /@[^_a-z]/i, warning: 'LittleHelper.Warning.BadVariable' }
	}
}

function checkItemFormulas(sheet, jq, options) {
	const formulaInputs = jq.find('input.formula');
	formulaInputs.each(function () {
		const formula = this.value;
		if (!(formula?.length > 0)) return;
		const warnings = [];
		for (let [name, rule] of Object.entries(rules.matchCount)) {
			let open = 0, close = 0;
			for (let l of formula) {
				if (l === rule.open) open++;
				if (l === rule.close) close++;
			}

			if (open !== close)
				warnings.push(game.i18n.localize(rule.warning).format(`"${rule.open}" ×${open}, "${rule.close}" ×${close}`));
		}
		for (let [name, rule] of Object.entries(rules.followups)) {
			if (rule.match.test(formula))
				warnings.push(game.i18n.localize(rule.warning));
		}

		if (warnings.length > 0) {
			const warning = warnings.join('\n');
			const symbol = addPostWarning($(this), warning);
			symbol.addClass('lil-formula-warning');
		}

	});
}

Hooks.on('renderItemSheet', checkItemFormulas);
