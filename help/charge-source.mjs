/*
import { Feature } from '../config.mjs';

new Feature('charge-source',
	'Charge source', 'Displays charge source in item charge configuration',
	'item',
	() => Hooks.on('renderItemSheetPF', injectChargeSource),
	() => Hooks.off('renderItemSheetPF', injectChargeSource));
*/

function injectChargeSource(sheet, jq, options) {
	const item = sheet.item;
	if (!item) return;
	if (!item.links.charges) return;

	const uses = jq.find('.uses-per select[name="data.uses.per"]');

	const itemLink = $('<div>').addClass('lil-charge-source').append(
		$('<img/>').attr({ src: item.links.charges.img }).addClass('lil-middling-content').css({width: '24px', height: '24px', flex: '0 24px'}),
		$('<label/>').text(item.links.charges.name).addClass('lil-charge-source-label lil-middling-content')
	);
	uses.parent().parent().after(
		$('<div>').addClass('form-group').append(
			$('<label/>').text(game.i18n.localize('Missing.PF1.ChargesSharedFrom')),
			itemLink
		)
	);
	itemLink.on('click', () => item.links.charges.sheet.render(true));
}

Hooks.on('renderItemSheetPF', injectChargeSource);
