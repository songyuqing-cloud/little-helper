import { isIncompatibleSheet } from '../common.mjs';

function naturalReachInjection(sheet, jq, data) {
	if (isIncompatibleSheet(sheet)) return;
	const reach = game.pf1.utils.convertDistance(sheet.actor.data.data.range.melee);
	jq.find('.tab.attributes [name="data.traits.stature"]')
		.before($('<label/>').addClass('unbold lil-box natural-reach').text(`${game.i18n.localize('Missing.PF1.NaturalReach')}: ${reach[0]} ${reach[1]}`));
}

Hooks.on('renderActorSheetPF', naturalReachInjection);
