import { enrichRoll, signNum } from '../common.mjs';

class Tag {
	check;
	success;
	failure;
	possible;
	content;
	hint;
	constructor(label, { hint = null, check = 0, success = false, possible = false, failure = false } = {}) {
		this.check = check;
		this.content = label;
		this.hint = hint;
		this.success = success;
		this.possible = possible;
		this.failure = failure;
	}
}

function rollAgain(ev) {
	const skillId = ev.target.dataset?.skillId ?? ev.target.parentElement?.dataset?.skillId,
		abilityId = ev.target.dataset?.abilityId ?? ev.target.parentElement?.dataset?.abilityId,
		saveId = ev.target.dataset?.saveId ?? ev.target.parentElement?.dataset?.saveId;

	const roll = skillId ? 'skill' : (abilityId ? 'ability' : (saveId ? 'save' : null));
	if (!roll) {
		ui.notifications.warn("Undefined roll received.");
		console.warn("ROLL | Undefined roll received");
	}

	let actors = canvas.tokens.controlled.map(t => t.actor).filter(a => a !== null);
	if (actors.length === 0 && game.user.character) actors = [game.user.character];

	const skipDialog = (game.settings.get("pf1", "skipActionDialogs") && !game.keyboard.isDown("Shift")) ||
		(!game.settings.get("pf1", "skipActionDialogs") && game.keyboard.isDown("Shift"));

	if (actors.length == 0) {
		console.warn("No actor selected to roll with.");
		ui.notifications.warn("No valid actor selected for rolling.");
		return;
	}

	for (let a of actors) {
		if (skillId)
			a.rollSkill(skillId, { skipDialog });
		else if (abilityId)
			a.rollAbility(abilityId, { skipDialog });
		else if (saveId)
			a.rollSavingThrow(saveId, { skipDialog });
	}
}

function appendCustomTags(jq, tags = []) {
	const div = document.createElement('div');
	div.classList.add('property-group', 'reminders', 'lil-check-notes');
	tags.forEach(t => {
		const tag = document.createElement('span');
		tag.classList.add('tag', 'enriched');
		tag.innerHTML = t.content;
		if (t.check) {
			tag.classList.add('check')
			if (t.success) tag.classList.add('success');
			if (t.possible) tag.classList.add('possible');
			if (t.failure) tag.classList.add('failure');
		}
		if (t.hint) {
			tag.classList.add('hint');
			tag.title = t.hint;
		}
		div.append(tag);
	});

	jq.get(0).getElementsByClassName('message-content')?.[0].append(div);
}

function handleCMbySubject(cm, jq, subject) {
	const check = cm.data.roll ? cm.roll.total : 0,
		tags = [];

	const skillId = subject.skill,
		abilityId = subject.ability,
		saveId = subject.save,
		otherId = subject.core;

	switch (skillId) {
		case 'apr':
			// Appraise
			tags.push(new Tag('Base DC 20', { check, failure: check < 10, possible: check >= 10 && check < 20, success: check >= 20 }));
			break;
		case 'spl':
			// Spellcraft
			// Display what level of spell the roll is good for.
			{
				const formula = '@check[Result] - 15[Base DC]';
				const roll = RollPF.safeRoll(formula, { check: check });
				const enriched = enrichRoll(roll, formula, roll.total).outerHTML;
				tags.push(new Tag('Identify max level: ' + enriched, { hint: "+ Perception DC modifiers.", check, possible: check >= 15, success: check > 33, failure: check < 15 }));
			}
			break;
		case 'dip':
			// Diplomacy
			tags.push(new Tag('Indifferent Base DC 15', { hint: "", check, failure: check < 5, possible: check >= 5 && check < 15, success: check > 15 }));
			break;
		case 'int':
			// Intimidate
			tags.push(
				new Tag('Demoralize DC: 10+HD+Wis', { check, failure: check < 10, possible: check >= 10 }),
				new Tag('±4/size', { hint: '±4 per size step difference.' })
			);
			break;
		case 'fly':
			// Fly
			tags.push(
				new Tag('Slow DC 10', { check, success: check >= 10, failure: check < 10 }),
				new Tag('Hover DC 15', { check, success: check >= 15, failure: check < 15 }),
				new Tag('Other DC 15/20', { check, success: check >= 20, possible: check >= 15 && check < 20, failure: check < 15 })
			);
			break;
		case 'acr':
			// Acrobatics
			tags.push(new Tag('Tumble DC=CMD+5', { hint: "CMD+5 for moving through occupied space.", check, failure: check < 0, possible: check >= 0 }));
			//tags.push({ content: 'Through DC=5+CMD', failure: check < 15, possible: check >= 15 }); // it's just +5 DC
			{
				const formula = 'floor(@check[Result] / 4)';
				const roll = RollPF.safeRoll(formula, { check: check });
				const enriched = enrichRoll(roll, formula, `${roll.total} ft`).outerHTML;
				tags.push(new Tag('Height: ' + enriched, { check, failure: roll.total <= 0, possible: roll.total > 0 }));
			}
			{
				const actor = game.actors.get(cm.data.speaker.actor);
				if (actor) {
					const formula = '@check[Base Distance] + floor((@attributes.speed.land.total - 30) / 10)[Speed Base] * 4[Distance Adjust]';
					const roll = RollPF.safeRoll(formula, { attributes: { speed: { land: { total: actor.data.data.attributes.speed.land.total } } }, check });
					const enriched = enrichRoll(roll, formula, `${roll.total} ft`).outerHTML;
					tags.push(new Tag('Length: ' + enriched, { check, failure: roll.total <= 0, possible: roll.total > 0 }));
				}
			}
			break;
		case 'blf':
			// Bluff
			tags.push(
				new Tag('Feint Base DC 10', { hint: "DC 10 + (BAB + Wis) or (Sense Motive), whichever is better.", check, failure: check < 10, possible: check >= 10 }),
				new Tag('Opposed by Sense Motive')
			);
			break;
		case 'clm': // Climb
			tags.push(new Tag('Natural Rock DC 15'));
			break;
		case 'dev': // Disable Device
			tags.push(
				new Tag('Device DC 10 or higher', { check, failure: check < 10, possible: check >= 10 }),
				new Tag('Lock DC 20 or higher', { check, failure: check < 20, possible: check >= 20 })
			);
			break;
		case 'dis': // Disguise
			tags.push(new Tag('Opposed by Perception'));
			break;
		case 'esc': // Escape Artist
			tags.push(new Tag('Escape DC 20 or higher', { check, failure: check < 20, possible: check >= 20 }));
			break;
		case 'han': // Handle Animal
			tags.push(new Tag('Base DC 10 or higher', { check, failure: check < 10, possible: check >= 10 }));
			break;
		case 'hea':
			// Heal
			tags.push(
				new Tag('Stabilize/Longterm DC 15', { check, success: check >= 15, possible: check >= 13 && check < 15, failure: check < 13 }),
				new Tag('Deadly DC 20', { hint: "+2 DC per missing healer's kit charge", check, success: check >= 24, possible: check >= 18 && check < 23, failure: check < 18 })
			);
			break;
		case 'rid':
			// Ride
			tags.push(new Tag('Fast (Dis)Mount DC 20', { check, failure: check < 20, success: check >= 20 }));
			break;
		case 'sen':
			// Sense Motive
			tags.push(
				new Tag('Base DC 20', { check, failure: check < 20, possible: check >= 20 }),
				new Tag('Opposed by Bluff')
			);
			break;
		case 'slt':
			// Sleight of Hand
			tags.push(
				new Tag('Palm DC 20', { check, failure: check < 10, possible: check >= 10 }),
				new Tag('Steal DC 20', { check, failure: check < 20, possible: check >= 20 }),
				new Tag('Opposed by Perception')
			);
			break;
		case 'per':
			// Perception
			tags.push(
				new Tag('Opposed by Stealth'),
				new Tag('Opposed by Sleight of Hand')
			);
			break;
		case 'ste':
			// Stealth
			tags.push(new Tag('Opposed by Perception'));
			break;
		case 'sur':
			// Survival
			tags.push(new Tag('Subsist DC 10', { hint: "+1 fed per 2 over", check, failure: check < 10, success: check >= 10 }));
			// TODO: Simply display how many are fed?
			break;
		case 'swim':
			tags.push(new Tag('Base DC 10', { check, failure: check < 10, possible: check >= 10 }));
			break;
		case 'umd':
			// Use Magic Device
			{
				const formula = '@check[Result] - 20[Base DC]';
				const roll = RollPF.safeRoll(formula, { check });
				const enriched = enrichRoll(roll, formula, roll.total).outerHTML;
				tags.push(new Tag('Wand DC 20', { check, failure: check < 20, possible: check >= 20 }));
				tags.push(new Tag('Scroll max level: ' + enriched, { hint: "Ignores need for emulated ability score.\nBase DC = 20 + Spell Level", check, possible: check >= 20, failure: check < 20, success: check >= 29 }));
			}
			break;
		case undefined:
			break;
		default:
			if (['kar', 'kdu', 'ken', 'kge', 'khi', 'klo', 'kna', 'kno', 'kpl', 'kre'].includes(skillId)) {
				tags.push(
					new Tag('Untrained DC 10', { hint: "Any common knowledge is DC 5 to 10 range.", check, success: check >= 10, failure: check < 5, possible: check < 10 && check >= 5 }),
					new Tag('Common Base DC 15', { hint: "Base DC usually ranges from 15 to 30 plus CR or level of the thing.", check, failure: check < 15, possible: check >= 15 && check < 33, success: check >= 33 })
				);
			}
			/*
			// craft skills
			// The DCs are all over the place (0 to 35 with no clear base
			}
			*/
			break;
	}

	switch (abilityId) {
		case 'str':
			// Strength
			tags.push(
				new Tag('Base DC 5', { check, failure: check < 5, possible: check >= 5 }),
				new Tag('Door DC 0–30', { hint: "Door DC varies from less than zero to around 30. Good luck.", check, possible: check < 30, success: check >= 30, failure: check < 0 })
			);
			break;
		case 'dex':
			// Dexterity
			tags.push(new Tag('Base DC 5', { check, failure: check < 5, possible: check >= 5 }));
			break;
		case 'con':
			// Constitution
			tags.push(new Tag('Base DC 5', { check, failure: check < 5, possible: check >= 5 }));
			{
				const actor = game.actors.get(cm.data.speaker.actor);
				if (actor) {
					const hp = duplicate(actor.data.data.attributes.hp);
					const eHP = hp.value + hp.temp;
					if (eHP < 0) {
						const formula = '10[Base] - (@hp.value + @hp.temp)[Effective HP]';
						const roll = RollPF.safeRoll(formula, { hp });
						const dc = roll.total;
						const enriched = enrichRoll(roll, formula, roll.total).outerHTML;
						tags.push(new Tag('Stabilize DC ' + enriched, { check, failure: dc > check, success: dc <= check }));
					}
					else {
						const b = actor.data.data.attributes.conditions.bleed;
						tags.push(new Tag('Stable', { check, success: !b, possible: b }));
					}
				}
			}
			break;
		case 'int':
			tags.push(new Tag('Base DC 5', { check, failure: check < 5, possible: check >= 5 }));
			break;
		case 'wis':
			tags.push(new Tag('Base DC 5', { check, failure: check < 5, possible: check >= 5 }));
			break;
		case 'cha':
			tags.push(new Tag('Base DC 10', { check, failure: check < 10, possible: check >= 10 }));
			break;
	}

	switch (otherId) {
		case 'concentration':
			{
				const formula = 'floor((@check[Result] - 15[Base DC]) / 2)';
				const roll = RollPF.safeRoll(formula, { check });
				const enriched = enrichRoll(roll, formula, roll.total).outerHTML;
				tags.push(
					new Tag('Defensive DC 15+SL×2', { check, failure: check < 15, possible: check >= 15, success: check > 33 }),
					new Tag('Max level: ' + enriched, { check, possible: check >= 15, success: check > 33, failure: check < 15 }),
					new Tag('Damaged DC 10+SL+DMG', { hint: "Continuous damage counts for only half.", check, failure: check < 10, possible: check >= 10 })
				);
			}
			break;
		case undefined:
			break;
	}

	switch (saveId) {
		case 'ref':
			tags.push(new Tag('Base DC 10', { check, failure: check < 10, possible: check >= 10 }));
			break;
		case 'fort':
			tags.push(new Tag('Base DC 10', { check, failure: check < 10, possible: check >= 10 }));
			break;
		case 'will':
			tags.push(new Tag('Base DC 10', { check, failure: check < 10, possible: check >= 10 }));
			break;
		case undefined:
			break;
	}
	if (tags.length > 0) appendCustomTags(jq, tags);

	/** Add me-too roller */
	if (skillId || abilityId || saveId) {
		const f = jq.find('.flavor-text');
		f.after(
			$('<span>').attr({ 'data-skill-id': skillId, 'data-ability-id': abilityId, 'data-save-id': saveId, 'data-core-id': otherId }).addClass('lil-roll-button')
				.append(
					$('<i>').addClass('fas fa-dice'),
					document.createTextNode('Roll')
				).on('click', rollAgain),
			$('<div>').addClass('kbwks-flow-break')
		);
	}
}

function handleMessage(cm, jq) {
	let subject = cm.data.flags?.pf1?.subject;
	// TODO: Option to disable old-style checks. Auto-disable with Foundry 0.9 or something.
	if (subject == undefined) subject = parseOldStyleCM(cm, jq);
	if (subject) handleCMbySubject(cm, jq, subject);
}

const skillMap = {
	'appraise': 'apr',
	'spellcraft': 'spl',
	'diplomacy': 'dip',
	'intimidate': 'int',
	'fly': 'fly',
	'acrobatics': 'acr',
	'bluff': 'blf',
	'climb': 'clm',
	'disable device': 'dev',
	'disguise': 'dis',
	'escape artist': 'esc',
	'handle animal': 'han',
	'heal': 'hea',
	'ride': 'rid',
	'sense motive': 'sen',
	'sleight of hand': 'slt',
	'perception': 'per',
	'stealth': 'ste',
	'survival': 'sur',
	'swim': 'swm',
	'use magic device': 'umd',
};

const saveMap = {
	reflex: 'ref',
	fortitude: 'fort',
	will: 'will',
};

const kSMap = {
	arcana: 'kar',
	dungeoneering: 'kdu',
	engineering: 'ken',
	geography: 'kge',
	historY: 'khi',
	local: 'klo',
	nature: 'kna',
	nobility: 'kno',
	planes: 'kpl',
	religion: 'kre',
};

function parseOldStyleCM(cm, jq) {
	const ftext = jq.find('.flavor-text')[0]?.textContent?.trim();
	if (!(ftext?.length > 0)) return; // no flavor

	let skillId, abilityId, saveId, otherId;
	if (/^(.*) skill check$/i.test(ftext)) {
		skillId = skillMap[RegExp.$1.toLowerCase()];
		if (skillId == undefined && /^knowledge \((.+)\)$/i.test(RegExp.$1))
			skillId = kSMap[RegExp.$1.toLowerCase()];
	}
	else if (/^(.*) ability test$/i.test(ftext))
		abilityId = RegExp.$1.substr(0, 3).toLowerCase();
	else if (/^(.*) saving throw$/i.test(ftext))
		saveId = saveMap[RegExp.$1.toLowerCase()];
	else {
		if (/^concentration check$/i.test(ftext))
			otherId = 'concentration';
	}

	const subject = {
		ability: abilityId,
		skill: skillId,
		core: otherId,
		save: saveId,
	};

	setProperty(cm.data.flags, "pf1.subject", subject);
	return subject;
}

Hooks.on('renderChatMessage', handleMessage);
