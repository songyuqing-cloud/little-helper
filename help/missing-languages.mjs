import { constructWarning } from '../common.mjs';

function addMissingLanguagesCounter(sheet, jq, _opts) {
	const actor = sheet.actor;
	const p = actor.data.data.traits.languages;
	const languages = [...p.value, ...p.custom.split(';').map(l => l?.trim()).filter(l => l?.length > 0)];
	const minLang = actor.data.data.abilities.int.mod + actor.data.data.skills.lin.rank + 1;

	if (languages.length < minLang) {
		const langs = jq.find('.tab.attributes [data-options="languages"]').closest('.form-group');
		const list = langs.find('.traits-list');
		list.append(constructWarning(`Too few languages. Should know at least ${minLang}. ${minLang - languages.length} missing.`))
			.addClass('lil-warning');
	}
}

Hooks.on('renderActorSheet', addMissingLanguagesCounter);
